var filteredProducts = []; // Lưu trữ danh sách sản phẩm đã lọc
function renderProductList(productArr) {
  var contentHTML = "";
  productArr.forEach(function (item) {
    //  forEach(function (item) là đi duyệt hàm
    // item đại diện cho từng phần tử trong productArr
    var content = `

  <div class="card" style="width: 20rem; margin-left: 50px; margin-top: 5px; margin-botton:5px; border-radius:15px;   overflow: hidden;">
  <img style= " margin-top: 15px"
  
   src="${item.img}"
    class="card-img-top"
    alt="${item.name}"
  />
  <div class="card-body">
  <h2 style="font-weight: bold;">${item.name} </h2>

  <div id="${item.price}" class="price-price">Giá: ${item.price}   </div>
  <div id="${item.screen}" class=" screen-screen font-semibold"> <span >${item.screen} inh</span></div>
  <span style="font-weight: bold;">Back Camera:</span> ${item.backCamera} <br />
  <span style="font-weight: bold;">Front Camera:</span> ${item.frontCamera} <br />
  <span style="font-weight: bold;">Description:</span> ${item.desc} <br />
  <span style="font-weight: bold;">Type:</span> ${item.type} <br />
  <span style="font-weight: bold;">Id:</span> ${item.id}
  </p>
    <a id= "button-cart" href="#"  style = "color: white" class="btn btn-info">vào giỏ hàng</a>
  </div>
</div>
  
  

    `;
    contentHTML += content;
    //
  });
  // <div id="${item.price}" class="price-price">Giá: ${item.price}   </div>

  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
  // show thông tin lên cart
  var addToCartButtons = document.querySelectorAll("#button-cart"); // button "thêm vào giỏ hàng"
  addToCartButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      // 1. lấy thông tinh sản phẩm
      //giúp bạn lấy phần tử cha của nút "thêm vào giỏ hàng". Điều này cho phép bạn truy cập đến các phần tử con bên trong phần tử cha đó.
      var productElement = this.parentNode;

      var ten = productElement.querySelector("h2");
      var gia = productElement.querySelector(".price-price");
      var manHinh = productElement.querySelector(".screen-screen");

      //  2. tạo object để add tất cả item( thuộc tính của phone) vào giỏ hàng
      var item = {
        name: ten.innerText,
        price: gia.innerText,
        screen: manHinh.innerText,
      };

      console.log("🚀 ~ file: controller.js:54 ~ item :", item);

      addToCart(item);
    });
  });
}
// funtion tăng giảm số lượng
function showCart() {
  var cartItemsHTML = [];
  var totalPrice = 0; // Biến lưu trữ tổng giá tiền
  cart.forEach(function (item) {
    var cartItemHTML = `
    <div style="color: black; border-radius: 5px; box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px; margin-top: 10px; margin-bottom: 10px;" class="cart-item">
    <span>Tên sản phẩm: ${item.name}</span><br />
    <span> ${item.price} $</span><br />
    <span>Màn Hình: ${item.screen}</span><br />
    <div class="quantity-buttons">
    
    <button style="background-color: rgb(241, 241, 241); border-radius:5px; border: none; color: black; transform: scale(1); transition: transform 0.2s;" class="quantity-button-plus" 
    onmouseover="this.style.transform = 'scale(1.2)'" 
    onmouseout="this.style.transform = 'scale(1)'"
  >+</button>
  
      <span class="quantity">${item.quantity}</span>
      <button style="background-color: rgb(241, 241, 241); border-radius:5px; border: none; color: black; transform: scale(1); transition: transform 0.2s;" class="quantity-button-minus" 
      onmouseover="this.style.transform = 'scale(1.2)'" 
      onmouseout="this.style.transform = 'scale(1)'"
    >-</button>
    
    </div>
    <button style="color: red;  background-color: transparent; border: none; transform: scale(1); transition: transform 0.2s;" data-index="${item.name}" class="button-remove"
    onmouseover="this.style.transform = 'scale(1.2)'"
    onmouseout="this.style.transform = 'scale(1)'"
  >remove</button>
  
  </div>
  
  
    `;

    cartItemsHTML.push(cartItemHTML);
    totalPrice += parseInt(item.price.replace(/[^0-9]/g, "")) * item.quantity; // Tính tổng giá tiền của từng sản phẩm
  });

  document.getElementById("total").innerHTML = "Total: " + totalPrice; // Hiển thị tổng giá tiền
  document.getElementById("product-cart-content").innerHTML = cartItemsHTML;
  document.getElementById("product-cart-content").innerHTML = cartItemsHTML.join(""); // xóa dấu ","" khi render 2 sản phẩm trở lên

  // theo dõi user khi user click vào nút "+" và "-"
  var plusButtons = document.querySelectorAll(".quantity-button-plus");
  var minusButtons = document.querySelectorAll(".quantity-button-minus");

  //  thì sau đó chạy function
  plusButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      // cần biết plusButtons thành array dùng Array.from(plusButtons)
      var itemIndex = Array.from(plusButtons).indexOf(button); // vì indexOf chỉ dùng được với array
      increaseQuantity(itemIndex);
    });
  });

  minusButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      var itemIndex = Array.from(minusButtons).indexOf(button);
      decreaseQuantity(itemIndex);
    });
  });

  // Lắng nghe sự kiện click vào nút "remove"
  var removeButtons = document.querySelectorAll(".button-remove");
  removeButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      var itemName = button.getAttribute("data-index");
      deleteProduct(itemName); // gọi đến function deleteProduct(index)  bên index.js
    });
  });

  console.log(cart); // Hiển thị thông tin giỏ hàng trong console
}
